{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE EmptyCase #-}
import Data.Map (Map)
import qualified Data.Map as Map

--Question 1

pRoduct :: [Int] -> Int
pRoduct xs = foldr (*) 1  xs

newMap :: Map Int Int -> [Int]->Map Int Int 
newMap y [] = y
newMap y (x:xs) = newMap(Map.insertWith (+) x 1 y) xs

myMaximum :: Ord a => [a] -> a
myMaximum = foldr1 (\a b ->if a >= b then a else b)

myMinimum :: Ord a => [a] -> a
myMinimum = foldr1 (\a b ->if a <= b then a else b)

card :: [Int] -> Map Int Int
card (x:xs) = newMap Map.empty (x:xs)


cardf :: [Int] -> Map Int Int
cardf  = foldr(\x counter ->(Map.insertWith (+) x 1 counter)) (Map.fromList [])


--filters out all keys with value (== maxVal) from the provided map. Note that maxVal returns the largest value found in the map.
maxElemMap :: Map Int Int ->[Int]
maxElemMap  xs =  Map.keys $ Map.filter (==maxVal ) xs
    where maxVal = maximum $  Map.elems xs
                    
mode ::[Int] -> [Int]
mode  x = maxElemMap (card x)



--Test Cases Q4
h_program1 = ["my_var" := (C 2), "my_var" := (C 5) `Minus` (V "my_var"), Return (V "my_var")]
h_program2 = [Def "f" h_program1, Return (Call "f" (C 0))]

--Question 2 and 3

type BytecodeProgram = [Inst]
data Const = ConstINT Int | ConstByteCode BytecodeProgram deriving(Show)
type PC = Int
type Var = String
type Globals = Map Var Const
acc :: Var
acc = "_acc"
data Inst = LOAD_CONST Const
  | LOAD_GLOBAL Var
  | STORE_GLOBAL Var 
  | CALL_FUNCTION 
  | JUMP PC
  | JUMP_IF_FALSE PC 
  | SUB Var deriving(Show)  


--exec takes an INST and returns the desired outcome of the instruction on the Map of globals and the program counter.
class Executable a where
 exec :: a -> (Globals, PC) -> Maybe(Globals, PC)

instance Executable Inst where
  exec(LOAD_CONST a)(b, c) = Just((Map.insert acc a b), c+1)--LOAD_CONST loads the constant 'a' as the value into the accumulator key.
  exec(LOAD_GLOBAL a) (b, c) = if (Map.member acc b) && (Map.member a b) then --LOAD_GLOBAL, loads the value of the key passed (provided it exists) into the accumulator. 
    Just ((Map.insert acc (b Map.! a) b), c+1)
    else Nothing
  exec(STORE_GLOBAL a) (b,c) = if (Map.member acc b) then-- STORE_GLOBAL stores the value in the accumulator in a global variable for which the key is passed as 'a'.
    Just ((Map.insert a (b Map.! acc) b), c+1)
    else Nothing
  exec(JUMP a) (b, c) = Just (b, c)
  exec(JUMP_IF_FALSE a) (b,c) = case  (b Map.! "_acc") of  
    ConstINT a  -> if (a ==  0) then exec(JUMP a) (b,c)
      else Just (b,c)
    ConstByteCode b -> Nothing
  exec(SUB a) (b, c) = if ((Map.member acc b) && (Map.member a b)) then --SUB takes a Var, provided a key matching the passed Var exists this value is subtracted from that of the acc.
     (case((b Map.! a),(b Map.! acc)) of
      ((ConstINT x),(ConstINT y)) -> Just ((Map.insert acc (ConstINT (y-x))  b), c+1)
      otherwise -> Nothing)
    else Nothing
  exec(CALL_FUNCTION)(b,c) =  case (b Map.! "_acc")  of --CALL_FUNCTION calls the ByteCodeProgram stored in the accumulator (provided one exists).
      ConstByteCode x -> Just(getGlobalSimpleExec (simpleExec x b),c+1)
      ConstINT y -> Nothing

--Merges simpleExec' with exec        
getGlobalExec :: Maybe(Globals, PC) -> Maybe Globals
getGlobalExec (Just(a, b)) = Just a
getGlobalExec Nothing = Nothing

--Returns Globals from Maybe Globals namely used on simpleExec.
getGlobalSimpleExec :: Maybe Globals -> Globals
getGlobalSimpleExec (Just a) = a

--simpleExec' goes through a bytecode program one inst at a time recursively running exec on each one and returning the updated globals mapping each time.
simpleExec' :: BytecodeProgram -> PC ->Maybe Globals -> Maybe  Globals
simpleExec' [] pc (Just b) = Just b
simpleExec' (x:xs) pc (Just b) = simpleExec' xs (pc+1) (getGlobalExec(exec x (b,pc)))
simpleExec' (x:xs) pc Nothing = Nothing
simpleExec' [] pc Nothing = Nothing
 
simpleExec :: BytecodeProgram -> Globals -> Maybe  Globals
simpleExec (x:xs) b = simpleExec' (x:xs) 0 (Just b)


--Question  4

infixl 5 :=
type HighLevelProgram = [Stmt]
data Stmt = Def Var HighLevelProgram
   -- Function declaration, e.g. Def "f" [...]
   | Var := Expr
   -- Assignment, e.g., "x" := Co 4
   | Return Expr
   -- Return from function
   | If Expr [Stmt]
   -- If Expr is not 0, execute statements
   | While Expr [Stmt]
   -- While Expr is not 0, execute statements 

data Expr = V Var -- An Identifier is an expression
  | C Int -- So is a constant
  | Call Var Expr -- And a function call
  | Minus Expr Expr -- Subtraction of two expressions
  deriving(Show)

class Evaluatable a where
 eval :: a  ->  Maybe a

instance Evaluatable Expr where
  eval(C a) = Just(C a)
  eval(V a) = Just (V a)
  eval(Call v exp) = case eval(exp) of
    Just(C a) -> Just(Call v (C a))
  eval (Minus e1 e2) = case (eval e1, eval e2) of
    (Just(C i1),Just(V i2)) -> Just(Minus (C i1) (V i2))
    otherwise -> Nothing

class Compile a where
  simpleComp :: a -> Maybe BytecodeProgram 

--Instance of compile working on the Stmt type.
instance Compile Stmt where
  simpleComp(Def v xs) = Just [LOAD_CONST (ConstByteCode (compile xs)),(STORE_GLOBAL v),(LOAD_GLOBAL v)] -- Function Declaration
  simpleComp(v := exp) = case eval(exp) of --Assignment of expression(Value) to a Var(Key)
   Just (C a) -> Just[LOAD_CONST (ConstINT a),(STORE_GLOBAL v),(LOAD_GLOBAL v)]
   Just (Minus (C a) (V b)) -> Just[LOAD_CONST (ConstINT a), SUB b]
   Just (V a) -> Nothing
  simpleComp(Return exp) = case eval(exp) of --Return from function
    Just (C a) -> Just [LOAD_CONST(ConstINT a)]
    Just (V a) -> Just [(STORE_GLOBAL a), (LOAD_GLOBAL a)]
    Just (Call v (C a)) -> Just[LOAD_GLOBAL v, CALL_FUNCTION]

--SimpleCompWrapper, simply returns BytecodeProgram from a Maybe Bytecode program to merge simpleComp to the compile function
simpleCompWrapper :: Maybe BytecodeProgram -> BytecodeProgram
simpleCompWrapper (Just b)= b
simpleCompWrapper Nothing = []

--HighlevelProgram to BytecodeProgram using simpleComp
compile :: HighLevelProgram -> BytecodeProgram
compile [] = []
compile (x:xs) =simpleCompWrapper(simpleComp(x)) ++ compile(xs)
    
